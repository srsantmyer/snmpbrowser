﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace snmpBrowser
{
    class DataUtils
    {
        internal static System.Data.DataSet NewDataSet(int tableCount, string columnNames)
        {
            DataSet r = new DataSet();

            try
            {
                DataTable t = new DataTable();
                string[] cols = columnNames.Split('|');
                if (cols.Length > 0)
                {
                    foreach (string s in cols)
                    {
                        t.Columns.Add(s);
                    }
                }
                r.Tables.Add(t);
            }
            catch(Exception ex)
            {
                // log
            }
            return r;
        }
    }
}
