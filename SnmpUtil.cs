﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnmpSharpNet;

namespace snmpBrowser
{
    public class SnmpUtil
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hostName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        internal static DataSet PollHost(string hostName, string password)
        {
            return GetBulk(hostName, password);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <param name="password"></param>
        private static DataSet GetBulk (string ipAddress, string password)
        {
            DataSet r = DataUtils.NewDataSet(1, "oid|value");

            AgentParameters param = new AgentParameters(SnmpVersion.Ver2, new OctetString(password));
            
            try
            {
                IpAddress agent = new IpAddress(ipAddress);
                UdpTarget target = new UdpTarget((System.Net.IPAddress)agent, 161, 2000, 1);
                Oid rootOid = new Oid("1.3.6.1");

                Oid lastOid = (Oid)rootOid.Clone();
                Pdu pdu = new Pdu(PduType.GetBulk);

                pdu.NonRepeaters = 0;
                pdu.MaxRepetitions = 20;

                while (lastOid != null)
                {
                    if (pdu.RequestId != 0)
                    {
                        pdu.RequestId += 1;
                    }
                    pdu.VbList.Clear();
                    pdu.VbList.Add(lastOid);

                    SnmpV2Packet result = (SnmpV2Packet)target.Request(pdu, param);
                    if (result != null)
                    {
                        if (result.Pdu.ErrorStatus != 0)
                        {
                            Console.WriteLine("Error in SNMP reply. Error {0} index {1}",
                            result.Pdu.ErrorStatus,
                            result.Pdu.ErrorIndex);
                            lastOid = null;
                            break;
                        }
                        else
                        {
                            foreach (Vb v in result.Pdu.VbList)
                            {
                                if (rootOid.IsRootOf(v.Oid))
                                {
                                    string deviceHCIDw = ipAddress;

                                    //  ostrm = new FileStream("snmp_dump.csv", FileMode.OpenOrCreate, FileAccess.Write);
                                    //  //  writer = new StreamWriter(ostrm);

                                    // Console.SetOut(writer);
                                    Console.WriteLine("{0} {1} ({2}): {3}",
                                    deviceHCIDw.ToString(),
                                    v.Oid.ToString(),
                                    SnmpConstants.GetTypeName(v.Value.Type),
                                    v.Value.ToString());
                                    // Console.SetOut(oldOut);
                                    // writer.Close();
                                    // ostrm.Close();
                                    Console.WriteLine("Done");

                                    DataRow nr = r.Tables[0].NewRow();
                                    nr["oid"] = v.Oid.ToString();
                                    nr["value"] = v.Value.ToString();
                                    r.Tables[0].Rows.Add(nr);                                    

                                    if (v.Value.Type == SnmpConstants.SMI_ENDOFMIBVIEW)
                                        lastOid = null;
                                    else
                                        lastOid = v.Oid;
                                }
                                else
                                {
                                    lastOid = null;
                                }
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("No response received from SNMP agent.");
                    }
                }
                target.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
            return r;
        }


        private static string InstanceToString(uint[] instance)
        {
            StringBuilder str = new StringBuilder();
            foreach (uint v in instance)
            {
                if (str.Length == 0)
                    str.Append(v);
                else
                    str.AppendFormat(".{0}", v);
            }
            return str.ToString();
        }


        /*
        private static void GetBulk(string host, string password)
        {
            Dictionary<String, Dictionary<uint, AsnType>> result = new Dictionary<String, Dictionary<uint, AsnType>>();
            List<uint> tableColumns = new List<uint>();
            string oid = string.Empty;
            oid = "1.3.6";
            IpAddress peer = new IpAddress();
            // Prepare agent information
            AgentParameters param = new AgentParameters(SnmpVersion.Ver2, new OctetString(password));
            if (TextUtils.IsIpAddress(host))
            {
                peer = new IpAddress(host);
            }
            else
            {
                System.Net.IPAddress[] ipList = System.Net.Dns.GetHostAddresses(host);
                foreach (System.Net.IPAddress ipa in ipList)
                {
                    if (!ipa.Address.ToString().StartsWith("127."))
                    {
                        peer = new IpAddress(ipa.Address.ToString());
                    }
                }
            }

            if (peer.Valid)
            {
                UdpTarget target = new UdpTarget((System.Net.IPAddress)peer);
                // This is the table OID supplied on the command line
                Oid startOid = new Oid(oid);
                // Each table OID is followed by .1 for the entry OID. Add it to the table OID
                startOid.Add(1); // Add Entry OID to the end of the table OID
                // Prepare the request PDU
                Pdu bulkPdu = Pdu.GetBulkPdu();
                bulkPdu.VbList.Add(startOid);
                // We don't need any NonRepeaters
                bulkPdu.NonRepeaters = 0;
                // Tune MaxRepetitions to the number best suited to retrive the data
                bulkPdu.MaxRepetitions = 100;
                // Current OID will keep track of the last retrieved OID and be used as 
                //  indication that we have reached end of table
                Oid curOid = (Oid)startOid.Clone();
                // Keep looping through results until end of table
                while (startOid.IsRootOf(curOid))
                {
                    SnmpPacket res = null;
                    try
                    {
                        res = target.Request(bulkPdu, param);
                        // Check if there is an agent error returned in the reply
                        if (res.Pdu.ErrorStatus == 0)
                        {
                            // Go through the VbList and check all replies
                            foreach (Vb v in res.Pdu.VbList)
                            {
                                curOid = (Oid)v.Oid.Clone();
                                // VbList could contain items that are past the end of the requested table.
                                // Make sure we are dealing with an OID that is part of the table
                                if (startOid.IsRootOf(v.Oid))
                                {
                                    // Get child Id's from the OID (past the table.entry sequence)
                                    uint[] childOids = Oid.GetChildIdentifiers(startOid, v.Oid);
                                    // Get the value instance and converted it to a dotted decimal
                                    //  string to use as key in result dictionary
                                    uint[] instance = new uint[childOids.Length - 1];
                                    Array.Copy(childOids, 1, instance, 0, childOids.Length - 1);
                                    String strInst = InstanceToString(instance);
                                    // Column id is the first value past <table oid>.entry in the response OID
                                    uint column = childOids[0];
                                    if (!tableColumns.Contains(column))
                                        tableColumns.Add(column);
                                    if (result.ContainsKey(strInst))
                                    {
                                        result[strInst][column] = (AsnType)v.Value.Clone();
                                    }
                                    else
                                    {
                                        result[strInst] = new Dictionary<uint, AsnType>();
                                        result[strInst][column] = (AsnType)v.Value.Clone();
                                    }
                                }
                                else
                                {
                                    // We've reached the end of the table. No point continuing the loop
                                    break;
                                }
                            }
                            // If last received OID is within the table, build next request
                            if (startOid.IsRootOf(curOid))
                            {
                                bulkPdu.VbList.Clear();
                                bulkPdu.VbList.Add(curOid);
                                bulkPdu.NonRepeaters = 0;
                                bulkPdu.MaxRepetitions = 100;
                            }
                        }
                        target.Close();
                        if (result.Count <= 0)
                        {
                            Console.WriteLine("No results returned.\n");
                        }
                        else
                        {
                            Console.Write("Instance");
                            foreach (uint column in tableColumns)
                            {
                                Console.Write("\tColumn id {0}", column);
                            }
                            Console.WriteLine("");
                            foreach (KeyValuePair<string, Dictionary<uint, AsnType>> kvp in result)
                            {
                                Console.Write("{0}", kvp.Key);
                                foreach (uint column in tableColumns)
                                {
                                    if (kvp.Value.ContainsKey(column))
                                    {
                                        Console.Write("\t{0} ({1})", kvp.Value[column].ToString(),
                                                          SnmpConstants.GetTypeName(kvp.Value[column].Type));
                                    }
                                    else
                                    {
                                        Console.Write("\t-");
                                    }
                                }
                                Console.WriteLine("");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Request failed: {0}", ex.Message);
                        target.Close();
                        break;
                    }
                }

            }
        }
         */
    }
}
