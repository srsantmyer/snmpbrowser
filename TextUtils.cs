﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace snmpBrowser
{
    public class TextUtils
    {
        /// <summary>
        /// Validate that this is a hostname or IP address
        /// </summary>
        /// <param name="hostName"></param>
        /// <returns></returns>
        internal static bool IsHostName(string hostName)
        {
            bool r = false;
            try
            {
                if(IsIpAddress(hostName.Trim()))
                {
                    r = true;
                }
                else
                {
                    // am I a hostname
                    try
                    {
                        string s = hostName.Trim();
                        if(!s.ToLower().StartsWith("http://"))
                        {
                            s = "http://" + s;
                        }
                        Uri uri = new Uri(s);
                        string d = string.Empty;
                        d = uri.Host; // will return www.foo.com
                        if(!string.IsNullOrWhiteSpace(d))
                        {
                            r = true;
                        }
                    }
                    catch
                    {
                        // logging
                    }
                }
            }
            catch
            {
                // logging
            }
            return r;
        }
        
        /// <summary>
        /// make sure I have some value
        /// </summary>
        /// <param name="textValue"></param>
        /// <returns></returns>
        internal static bool HasText(string textValue)
        {
            return !string.IsNullOrWhiteSpace(textValue);
        }

        /// <summary>
        /// am I a number?
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        internal static bool IsNumber(string s)
        {
            bool r = false;
            float f;
            r = float.TryParse(s, out f);
            return r;
        }

        /// <summary>
        /// am I a valid System.Net.IPAddress IP Address?
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        internal static bool IsIpAddress(string s)
        {
            bool r = false;
            System.Net.IPAddress a;
            r = System.Net.IPAddress.TryParse(s, out a);
            return r;
        }
    }
}
